const del = require('del');

const include = [
  'client/src',
  'client/public',
  'client/node_modules/**',
  'database/**',
  'docs/**',
  'scripts/**',
  '**/__tests__/**',
  '**/*.test.js',
  '**/test.js',
  '**/*.md',
  '.gitlab-ci.yml'
];
const exclude = ['node_modules/**'];
const files = include.concat(exclude.map(path => `!${path}`));
const testEnv = process.env.NODE_ENV !== 'production';

(async () => {
  const deletedPaths = await del(files, { dryRun: testEnv });
  console.log(deletedPaths.join('\n'), '\nDeleted all files');
})();
