const fs = require('fs');
const path = require('path');
const keys = require('../server/util/keygen');

const rootDir = path.resolve();
const keysDir = path.join(rootDir, 'keys');
const keysDirExists = fs.existsSync(keysDir);
if (keysDirExists) {
  process.exit(0);
  /* try {
    fs.rmdirSync(keysDir);
  } catch (err) {
    if (err.code === 'ENOTEMPTY') {
      fs.readdirSync(keysDir).forEach(file =>
        fs.unlinkSync(path.join(keysDir, file))
      );
    }
  } finally {
    if (fs.existsSync(keysDir)) {
      fs.rmdirSync(keysDir);
    }
  } */
}
fs.mkdirSync(keysDir);

const privateKeyFilepath = path.join(keysDir, 'private.pem');
const publicKeyFilepath = path.join(keysDir, 'public.pub');

console.log('Generating RSA key pair...');
fs.writeFileSync(privateKeyFilepath, keys.privateKey);
fs.writeFileSync(publicKeyFilepath, keys.publicKey);
console.log('RSA key pair generation complete');
