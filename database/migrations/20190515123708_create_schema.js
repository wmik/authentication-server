exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('accounts', function(table) {
      table.increments('account_id').primary();
      table
        .string('email', 30)
        .unique()
        .notNullable();
      table.text('password_hash').notNullable();
      table.jsonb('roles').notNullable();
      table.timestamps(true, true);
    })
    .createTable('clients', function(table) {
      table
        .string('client_id')
        .primary()
        .notNullable();
      table.text('client_secret_hash').notNullable();
      table.jsonb('grants').notNullable();
      table.string('scope').notNullable();
      table
        .integer('account_id')
        .unsigned()
        .notNullable()
        .index();
      table
        .foreign('account_id')
        .references('account_id')
        .inTable('accounts')
        .onDelete('cascade');
      table.timestamps(true, true);
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('clients').dropTableIfExists('accounts');
};
