const data = require('../data/mock_data');

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return Promise.join(
    knex('accounts').del(),
    knex('accounts').insert(data.accounts)
  ).then(() =>
    Promise.join(knex('clients').del(), knex('clients').insert(data.clients))
  );
};
