# Installation and setup guide

## Prerequisites
|Technology|Version|
|-|-|
|PostgreSQL|`>= 9.5`|
|NodeJS|`>= 10.15.1`|
|NPM|`>= 6.4.1`|

## Installation

1. Clone the repository

        git clone <repo_url>

2. Install required dependencies using npm or yarn

        npm install or yarn install

## Setup
1. Configure required environment variables

    - `POSTGRES_USER` - PostgreSQL username
    - `POSTGRES_PASSWORD` - PostgreSQL password
    - `POSTGRES_DB` - PostgreSQL database name
    - `NODE_ENV` - Operating environment *(Used to match configuration files by `config` dependency)*

    > For quick start set all the variables to `test`. Ensure the PostgreSQL database and user with password are also setup before hand.

2. Start the application in development mode

        npm run watch
      
    > Set the `DEBUG` environment variable to view logs in the console. `namespace = app:*`

