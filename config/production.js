const createError = require('http-errors');

module.exports = {
  cors: {
    whitelist: process.env.CORS_WHITELIST.split(','),
    origin: function(whitelist) {
      return function(origin, cb) {
        if (whitelist.indexOf(origin) !== -1) {
          return cb(null, true);
        }
        return cb(
          createError(400, {
            errors: [
              {
                location: 'header',
                param: 'Origin',
                msg: `Origin ${origin} is not allowed by Access-Control-Allow-Origin`
              }
            ]
          })
        );
      };
    }
  },
  jwt: {
    ISSUER: '',
    AUDIENCE: '',
    SUBJECT: '',
    EXPIRY: '30d',
    ALGORITHM: 'RS256'
  }
};
