const createError = require('http-errors');
module.exports = {
  cors: {
    whitelist: ['127.0.0.1', 'localhost'],
    origin: function(whitelist) {
      return function(origin, cb) {
        if (whitelist.indexOf(origin) !== -1) {
          return cb(null, true);
        }
        return cb(
          createError(400, {
            errors: [
              {
                location: 'header',
                param: 'Origin',
                msg: `Origin ${origin} is not allowed by Access-Control-Allow-Origin`
              }
            ]
          })
        );
      };
    }
  },
  jwt: {
    ISSUER: '',
    AUDIENCE: '',
    SUBJECT: '',
    EXPIRY: '30d',
    ALGORITHM: 'RS256'
  }
};
