module.exports = {
  apps: [
    {
      name: 'oauth-server',
      script: './.bin/www',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }
  ]
};
