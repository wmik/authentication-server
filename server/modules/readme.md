> # modules

Application modules

## Folder structure

|Module|Description|
|-|-|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [accounts](accounts)|Accounts module.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [clients](clients)|Clients module.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [oauth](oauth)|OAuth module.|
