const express = require('express');
const createError = require('http-errors');
const expressAsyncMiddleware = require('express-async-handler');
const { basicAuth, missingAuthHeader, validate } = require('../../validators');
const { fromBasicScheme } = require('../../util/credentials');
const accounts = require('../accounts');
const clients = require('../clients');
const jwt = require('../../services/jwt');

const router = express.Router();

function login(req, res, next) {
  const errors = validate(req);
  if (errors !== null) {
    return next(errors);
  }
  const { authorization } = req.headers;
  const token = authorization.replace(/[Bb]earer\s+/, '');
  const account = jwt.verify(token);
  if (!account) {
    next(createError(401));
  }
  const data = { account };
  return res.json({ data });
}

async function createAccessToken(req, res, next) {
  const errors = validate(req);
  if (errors !== null) {
    return next(errors);
  }
  const { authorization } = req.headers;
  const [username, password] = fromBasicScheme(authorization);
  const client = await clients.db.authenticate(username, password);
  if (client) {
    const accountsDBResults = await accounts.db.getAccount({
      account_id: client.account_id
    });
    if (accountsDBResults && accountsDBResults.length > 0) {
      const [account] = accountsDBResults;
      delete account.password_hash;
      const accessToken = jwt.sign(account);
      const tokenType = 'bearer';
      const data = { token_type: tokenType, access_token: accessToken };
      return res.json({ data });
    }
  }
  return next(
    createError(422, {
      errors: [
        {
          location: 'header',
          params: ['Authorization'],
          msg: 'Invalid client credentials'
        }
      ]
    })
  );
}

router.get('/login', missingAuthHeader.checks, login);

router.post(
  '/tokens',
  basicAuth.checks,
  expressAsyncMiddleware(createAccessToken)
);

module.exports = router;
