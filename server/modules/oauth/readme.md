> # oauth

OAuth module

## Folder structure

|Module|Description|
|-|-|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [oauth.route](oauth.route.js)|OAuth route.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [index](index.js)|OAuth module entry root.|

## Routes

|endpoint|method|headers|response|
|-|-|-|-|
|`/oauth/tokens`|*POST*|Authorization,<p style="margin:0; text-indent: 20px;">`scheme`: *Basic*</p>| ***200 :*** `{"data":{"access_token": "string", "token_type": "bearer" }}` <br> ***422 :*** `{"errors": [], "data": "null" }`|
|`/oauth/login`|*POST*|Authorization,<p style="margin:0; text-indent: 20px;">`scheme`: *Bearer*</p>| ***200 :*** `{"data":{"account": {}}` <br> ***422 :*** `{"errors": [], "data": "null" }`|
