import test from 'ava';
import request from 'supertest';
import app from '../../';
import { encode } from '../../util/base64';

test.before(async t => {
  t.context.client = { client_id: 'randomBytes', client_secret: 'pass123' };
});

test('it should respond with an access token given valid client credentials', async t => {
  const { client_id, client_secret } = t.context.client;
  const response = await request(app)
    .post('/oauth/tokens')
    .set('Authorization', `Basic ${encode(`${client_id}:${client_secret}`)}`);
  t.true(response.ok);
  t.true('data' in response.body);
});
