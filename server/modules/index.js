const accounts = require('./accounts');
const clients = require('./clients');
const login = require('./login');
const oauth = require('./oauth');

module.exports = { accounts, clients, login, oauth };
