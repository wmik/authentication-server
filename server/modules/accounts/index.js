const db = require('./accounts.db');
const router = require('./accounts.route');

module.exports = {
  db,
  router
};
