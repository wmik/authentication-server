const express = require('express');
const expressAsyncMiddleware = require('express-async-handler');
const { basicAuth, validate } = require('../../validators');
const { fromBasicScheme } = require('../../util/credentials');
const accountsDB = require('./accounts.db');

const router = express.Router();

async function createAccount(req, res, next) {
  const errors = validate(req);
  if (errors !== null) {
    next(errors);
  }
  const { authorization } = req.headers;
  const [username, password] = fromBasicScheme(authorization);
  const accountsDBResults = await accountsDB.saveAccount({
    email: username,
    password
  });
  if (accountsDBResults && accountsDBResults.length > 0) {
    const [account] = accountsDBResults;
    const data = { account };
    return res.json({ data });
  }
  return next(err);
}

router.post('/', basicAuth.checks, expressAsyncMiddleware(createAccount));

module.exports = router;
