import test from 'ava';
import * as accountsDB from '../accounts.db';

test.before(t => {
  const email = 'user@local.test';
  const password = 'pass123';
  const roles = ['user'];
  t.context.account = { email, password, roles };
});

test.after(async t => {
  const { email } = t.context.account;
  await accountsDB.deleteAccount({ email });
});

test.serial(
  'it should create an account successfully given valid arguments',
  async t => {
    const { email, password } = t.context.account;
    const [account] = await accountsDB.saveAccount({ email, password });
    t.is(account.email, t.context.account.email);
    t.deepEqual(account.roles, t.context.account.roles);
  }
);

test.serial(
  'it should fail to create an account given duplicate credentials',
  async t => {
    const { email, password } = t.context.account;
    await t.throwsAsync(
      () => accountsDB.saveAccount({ email, password }),
      `Key (email)=(${email}) already exists.`
    );
  }
);

test.serial(
  'it should respond with an empty array if an account does not exist',
  async t => {
    const accounts = await accountsDB.getAccount({ email: 'doesnotexist' });
    t.deepEqual(accounts, []);
  }
);

test.serial(
  'it should respond with an error given invalid credentials',
  async t => {
    const account = await accountsDB.authenticate(
      t.context.account.email,
      'invalid_password'
    );
    t.is(account, null);
  }
);
