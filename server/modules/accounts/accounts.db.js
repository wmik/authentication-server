const Knex = require('knex');
const bcrypt = require('bcryptjs');
const knexConfig = require('../../../config/knex.config');

const config = knexConfig[process.env.NODE_ENV];
const knex = Knex(config);

const tableName = 'accounts';

async function saveAccount(account) {
  const saltRounds = await bcrypt.genSalt();
  const password_hash = await bcrypt.hash(account.password, saltRounds);
  const roles = JSON.stringify(['user']);
  return knex
    .returning(['email', 'roles'])
    .insert({ email: account.email, password_hash, roles })
    .into(tableName)
    .catch(err => {
      throw new Error(err.detail);
    });
}

function getAccount(account) {
  return knex
    .select()
    .from(tableName)
    .where(account);
}

async function authenticate(email, password) {
  const [account] = await getAccount({ email });
  let valid = false;
  if (account) {
    valid = await bcrypt.compare(password, account.password_hash);
    if (valid) {
      delete account.password_hash;
      return account;
    }
  }
  return null;
}

function deleteAccount(account) {
  return knex
    .delete()
    .from(tableName)
    .where(account);
}

module.exports = {
  authenticate,
  saveAccount,
  getAccount,
  deleteAccount
};
