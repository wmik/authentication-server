> # accounts

Accounts module

## Folder structure

|Module|Description|
|-|-|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [accounts.db](accounts.db.js)|Data access object.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [accounts.route](accounts.route.js)|Accounts route.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [index](index.js)|Accounts module entry root.|

## Routes

|endpoint|method|headers|response|
|-|-|-|-|
|`/accounts`|*POST*|Authorization,<p style="margin:0; text-indent: 20px;">`scheme`: *Basic*</p>| ***200 :*** `{"data":{"account": {}}` <br> ***422 :*** `{"errors": [], "data": "null" }`|

## Database structure
```
                                        Table "public.accounts"
    Column     |           Type           |                           Modifiers
---------------+--------------------------+---------------------------------------------------------------
 account_id    | integer                  | not null default nextval('accounts_account_id_seq'::regclass)
 email         | character varying(30)    | not null
 password_hash | text                     | not null
 roles         | jsonb                    | not null
 created_at    | timestamp with time zone | not null default now()
 updated_at    | timestamp with time zone | not null default now()
Indexes:
    "accounts_pkey" PRIMARY KEY, btree (account_id)
    "accounts_email_unique" UNIQUE CONSTRAINT, btree (email)
Referenced by:
    TABLE "clients" CONSTRAINT "clients_account_id_foreign" FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON DELETE CASCADE
```
