> # login

Login module

## Folder structure

|Module|Description|
|-|-|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [login.route](login.route.js)|Login route.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [index](index.js)|Login module entry root.|

## Routes

|endpoint|method|headers|response|
|-|-|-|-|
|`/login`|*POST*|Authorization,<p style="margin:0; text-indent: 20px;">`scheme`: *Basic*</p>| ***200 :*** `{"data":{"account": {} }}` <br> ***422 :*** `{"errors": [], "data": "null" }`|
