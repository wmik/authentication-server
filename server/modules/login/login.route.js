const createError = require('http-errors');
const express = require('express');
const expressAsyncMiddleware = require('express-async-handler');
const { basicAuth, validate } = require('../../validators');
const { fromBasicScheme } = require('../../util/credentials');
const accounts = require('../accounts');

const router = express.Router();

async function login(req, res, next) {
  const errors = validate(req);
  if (errors !== null) {
    return next(errors);
  }
  const { authorization } = req.headers;
  const [username, password] = fromBasicScheme(authorization);
  try {
    const account = await accounts.db.authenticate(username, password);
    if (account) {
      return res.json({ data: { account } });
    }
    next(createError(401));
  } catch (err) {
    next(err);
  }
}

router.post('/', basicAuth.checks, expressAsyncMiddleware(login));

module.exports = router;
