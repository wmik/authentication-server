const db = require('./clients.db');
const router = require('./clients.route');

module.exports = {
  db,
  router
};
