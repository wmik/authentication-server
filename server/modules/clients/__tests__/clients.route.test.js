import test from 'ava';
import request from 'supertest';
import { encode } from '../../../util/base64';
import app from '../../../';

test('it should respond with a bad request error given a request with a missing/invalid authorization header', async t => {
  const response = await request(app).post('/clients');
  t.false(response.ok);
  t.is(response.status, 400);
});

test('it should respond with client info given a valid request', async t => {
  const response = await request(app)
    .post('/clients')
    .set('Authorization', `Basic ${encode('admin@local.root:pass123')}`);
  t.true(response.ok);
});
