import test from 'ava';
import * as clientsDB from '../clients.db';

test('it should create a client given valid arguments', async t => {
  const client = await clientsDB.saveClient(1, 'read');
  t.true('client_id' in client);
  t.true('client_secret' in client);
});

test('it should authenticate a client given valid credentials', async t => {
  const _client = await clientsDB.saveClient(1, 'read');
  const client = await clientsDB.authenticate(
    _client.client_id,
    _client.client_secret
  );
  t.is(client.client_id, _client.client_id);
  t.is(client.scope, 'read');
  t.deepEqual(client.grants, ['client_credentials']);
});
