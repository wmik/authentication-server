> # clients

Clients module

## Folder structure

|Module|Description|
|-|-|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [clients.db](clients.db.js)|Data access object.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [clients.route](clients.route.js)|Clients route.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [index](index.js)|Clients module entry root.|

## Routes

|endpoint|method|headers|response|
|-|-|-|-|
|`/clients`|*POST*|Authorization,<p style="margin:0; text-indent: 20px;">`scheme`: *Basic*</p>| ***200 :*** `{"data":{"client": {} }}` <br> ***422 :*** `{"errors": [], "data": "null" }`|

## Database structure
```
                                         Table "public.clients"
    Column     |           Type           |                           Modifiers
---------------+--------------------------+---------------------------------------------------------------
 client_id          | character varying(255)   | not null
 client_secret_hash | text                     | not null
 grants             | jsonb                    | not null
 scope              | character varying(255)   | not null
 account_id         | integer                  | not null
 created_at         | timestamp with time zone | not null default now()
 updated_at         | timestamp with time zone | not null default now()
Indexes:
    "clients_pkey" PRIMARY KEY, btree (client_id)
    "clients_account_id_index" btree (account_id)
Foreign-key constraints:
    "clients_account_id_foreign" FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON DELETE CASCADE
```
