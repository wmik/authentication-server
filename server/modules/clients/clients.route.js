const express = require('express');
const createError = require('http-errors');
const expressAsyncMiddleware = require('express-async-handler');
const { fromBasicScheme } = require('../../util/credentials');
const { basicAuth, validate } = require('../../validators');
const accounts = require('../accounts');
const clientsDB = require('./clients.db');

const router = express.Router();

async function createClient(req, res, next) {
  const errors = validate(req);
  if (errors !== null) {
    next(errors);
  }
  const { authorization } = req.headers;
  const [username, password] = fromBasicScheme(authorization);
  const account = await accounts.db.authenticate(username, password);
  if (account) {
    const scope = `read ${
      account.roles.includes('admin') ? 'write' : ''
    }`.trim();
    const client = await clientsDB.saveClient(account.account_id, scope);
    const data = { client };
    return res.json({ data });
  }
  return next(
    createError(422, {
      errors: [
        {
          location: 'header',
          params: ['Authorization'],
          msg: 'Invalid credentials'
        }
      ]
    })
  );
}

router.post('/', basicAuth.checks, expressAsyncMiddleware(createClient));

module.exports = router;
