const crypto = require('crypto');
const { promisify } = require('util');
const Knex = require('knex');
const bcrypt = require('bcryptjs');
const knexConfig = require('../../../config/knex.config');

const config = knexConfig[process.env.NODE_ENV];
const knex = Knex(config);

const tableName = 'clients';
const randomBytesAsync = promisify(crypto.randomBytes);

function generateRandomBytes() {
  return randomBytesAsync(256).then(function(buffer) {
    return crypto
      .createHash('sha1')
      .update(buffer)
      .digest('hex');
  });
}

async function countClients(transaction, account_id) {
  const results = await transaction
    .select()
    .from(tableName)
    .where({ account_id })
    .count();
  if (results && results.length > 0) {
    const [row] = results;
    return parseInt(row.count);
  }
}

async function saveClient(account_id, scope) {
  const client_id = await generateRandomBytes();
  const client_secret = await generateRandomBytes();
  const saltRounds = await bcrypt.genSalt();
  const client_secret_hash = await bcrypt.hash(client_secret, saltRounds);
  const grants = JSON.stringify(['client_credentials']);
  try {
    await knex.transaction(async trx => {
      await knex(tableName)
        .transacting(trx)
        .forUpdate();
      const count = await countClients(trx, account_id);
      if (count === 5) {
        const err = new Error();
        err.detail = 'maximum client count reached';
        throw err;
      }
      await trx
        .insert({ client_id, client_secret_hash, scope, grants, account_id })
        .into(tableName);
    });
    return { client_id, client_secret };
  } catch (err) {
    throw new Error(err.detail);
  }
}

function getClient(client) {
  const getQuery = knex.select().from(tableName);
  if (client === null) {
    return getQuery;
  }
  return getQuery.where(client);
}

async function authenticate(clientId, clientSecret) {
  const [client] = await getClient({ client_id: clientId });
  let valid = false;
  if (client) {
    valid = await bcrypt.compare(clientSecret, client.client_secret_hash);
    if (valid) {
      delete client.client_secret_hash;
      return client;
    }
  }
  return valid;
}

module.exports = {
  authenticate,
  saveClient,
  getClient
};
