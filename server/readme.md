> # app

Application entry point

## Folder structure

|Module|Description|
|-|-|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [middleware](middleware)|Application middleware.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [modules](modules)|Application modules.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [services](services)|Application services.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [util](util)|Application utilities.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [index](index.js)|Application entry root.|
