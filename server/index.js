const path = require('path');
const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const errorsMiddleware = require('./middleware/errors');
const { accounts, clients, login, oauth } = require('./modules');

const app = express();

const isProduction = process.env.NODE_ENV.includes('prod');
const clientDir = path.resolve('client', 'build');

app.use(helmet());
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(clientDir));

app.all('/', function httpsRedirect(req, res, next) {
  if (!isProduction || req.secure) {
    return next();
  }
  const location = `https://${req.headers.host}${req.url}`;
  res.redirect(308, location);
});

app.get('/status', (req, res) => res.send('ok'));

app.use('/login', login.router);
app.use('/oauth', oauth.router);
app.use('/accounts', accounts.router);
app.use('/clients', clients.router);

// This definition needs to be the last one
// before error handler
// to support client side routing
// any get request that the server doesn't respond to
// is assumed to be a client side request
app.get('/*', (req, res) => {
  res.setHeader('Content-Type', 'text/html');
  res.sendFile(path.join(clientDir, 'index.html'));
});
app.use(errorsMiddleware());

module.exports = app;
