const createError = require('http-errors');
const { header, validationResult } = require('express-validator/check');

const missingAuthHeader = {
  checks: [
    header(
      'Authorization',
      'Invalid request. Missing authorization header'
    ).exists()
  ]
};

const missingAuthScheme = {
  checks: [
    header(
      'Authorization',
      'Invalid authentication scheme. Required `Basic` scheme'
    ).contains('Basic')
  ]
};

const basicAuth = {
  checks: missingAuthHeader.checks.concat(missingAuthScheme.checks)
};

function validate(req) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return createError(400, { errors: errors.array() });
  }
  return null;
}

module.exports = {
  missingAuthHeader,
  missingAuthScheme,
  basicAuth,
  validate
};
