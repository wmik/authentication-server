function shallowMerge(a, b) {
  return Object.assign({}, a, b);
}

module.exports = { shallowMerge };
