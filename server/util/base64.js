function encode(string) {
  return Buffer.from(string).toString('base64');
}

function decode(b64) {
  return Buffer.from(b64, 'base64').toString('ascii');
}

module.exports = { encode, decode };
