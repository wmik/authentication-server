const { decode } = require('./base64');

function fromScheme(scheme) {
  return function(string) {
    return string.replace(scheme, '');
  };
}

function fromBasicScheme(string) {
  return decode(fromScheme(/Basic\s+/)(string)).split(':');
}

function fromBearerScheme(string) {
  return fromScheme(/Bearer\s+/)(string);
}

module.exports = {
  fromScheme,
  fromBasicScheme,
  fromBearerScheme
};
