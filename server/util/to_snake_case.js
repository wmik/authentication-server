function to_snake_case_string(camelCaseString) {
  return camelCaseString.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
}

function to_snake_case_object_keys(objectWithCamelCase) {
  const parsedObject = {};
  const keys = Object.keys(objectWithCamelCase);
  for (const key of keys) {
    parsedObject[to_snake_case_string(key)] = objectWithCamelCase[key];
  }
  return parsedObject;
}

function to_snake_case_object_values(objectWithCamelCase) {
  const parsedObject = {};
  const keys = Object.keys(objectWithCamelCase);
  for (const key of keys) {
    parsedObject[key] = to_snake_case_string(objectWithCamelCase[key]);
  }
  return parsedObject;
}

function to_snake_case_object(objectWithCamelCase, convert) {
  let parsedObject = null;
  if (convert.includes('key')) {
    parsedObject = to_snake_case_object_keys(objectWithCamelCase);
  }
  if (convert.includes('val')) {
    if (parsedObject !== null) {
      return to_snake_case_object_values(parsedObject);
    }
    return to_snake_case_object_values(objectWithCamelCase);
  }
  return parsedObject || objectWithCamelCase;
}

module.exports = {
  string: to_snake_case_string,
  object: to_snake_case_object,
  keys: to_snake_case_object_keys,
  values: to_snake_case_object_values
};
