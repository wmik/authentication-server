import test from 'ava';
import { shallowMerge } from '.';

test('it should perform a shallow merge given valid arguments', t => {
  const a = { a: 1 };
  const b = { b: 2 };
  t.deepEqual(shallowMerge(a, b), Object.assign({}, a, b));
});
