import test from 'ava';
import keygen from '../keygen';

test('it generates a private key', t => {
  t.true(keygen.privateKey.includes('PRIVATE KEY'));
});

test('it generates a public key', t => {
  t.true(keygen.publicKey.includes('PUBLIC KEY'));
});
