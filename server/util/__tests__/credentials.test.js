import test from 'ava';
import * as base64 from '../base64';
import * as credentials from '../credentials';

test.before(t => {
  const username = 'user';
  const password = 'p@55w0rD';
  const plainCredentials = `${username}:${password}`;
  t.context.request = {
    scheme: { basic: 'Basic', bearer: 'Bearer' },
    encodedCredentials: base64.encode(plainCredentials),
    token: 't0k3N',
    username,
    password,
    plainCredentials
  };
});

test('it should obtain credentials given authorization header with specified scheme', t => {
  const { encodedCredentials, scheme } = t.context.request;
  const header = `${scheme.basic} ${encodedCredentials}`;
  t.is(credentials.fromScheme(/Basic\s+/)(header), encodedCredentials);
});

test('it should return array credentials given authorization header with `Basic` scheme', t => {
  const { username, password, scheme, encodedCredentials } = t.context.request;
  const header = `${scheme.basic} ${encodedCredentials}`;
  t.deepEqual(credentials.fromBasicScheme(header), [username, password]);
});

test('it should return a token given authorization header with `Bearer` scheme', t => {
  const { scheme, token } = t.context.request;
  const header = `${scheme.bearer} ${token}`;
  t.is(credentials.fromBearerScheme(header), token);
});
