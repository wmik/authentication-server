import test from 'ava';
import * as to_snake_case from '../to_snake_case';

test('it should convert a string to_snake_case given camelCase', t => {
  t.is(to_snake_case.string('camelCaseString'), 'camel_case_string');
});

test('it should convert object properties to_snake_case given camelCase object properties', t => {
  t.deepEqual(to_snake_case.object({ a: 'camelCaseString' }, 'values'), {
    a: 'camel_case_string'
  });
  t.deepEqual(to_snake_case.object({ camelCaseString: 'a' }, 'keys'), {
    camel_case_string: 'a'
  });
  t.deepEqual(
    to_snake_case.object({ camelCaseString: 'camelCaseString' }, 'values,keys'),
    {
      camel_case_string: 'camel_case_string'
    }
  );
});
