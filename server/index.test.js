import test from 'ava';
import request from 'supertest';
import app from '.';

test('it should return ok given a request to `/status` endpoint', async t => {
  const response = await request(app).get('/status');
  t.true(response.ok);
  t.is(response.text, 'ok');
});
