const debug = require('debug')('app:middleware:errors');
const createError = require('http-errors');

function generateError(location, params, msg) {
  return {
    location,
    params,
    msg
  };
}

function generateUsernameError(msg) {
  return generateError('header', 'username', msg);
}

module.exports = () => (err, req, res, next) => {
  debug(err);
  if (err instanceof createError.UnprocessableEntity) {
    return res.status(422).json({ data: null, errors: err.errors });
  }
  if (err instanceof createError.Unauthorized) {
    return res.status(401).json({
      data: null,
      errors: [generateError('header', 'authorization', 'Invalid token')]
    });
  }
  if (err instanceof createError.BadRequest) {
    return res.status(400).json({ data: null, errors: err.errors });
  }
  if (err.message.includes('exists')) {
    return res.status(422).json({
      data: null,
      errors: [generateUsernameError('email already registered to account.')]
    });
  }
  if (err.message.includes('maximum client count')) {
    return res.status(422).json({
      data: null,
      errors: [generateUsernameError(err.message)]
    });
  }
  res.status(500).send('Internal Server Error!');
};
