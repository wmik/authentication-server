import test from 'ava';
import jwt from './jwt';
import config from 'config';

test.before(t => {
  t.context.payload = { id: 1 };
});

test('it should sign a payload given valid arguments', t => {
  const token = jwt.sign(t.context.payload);
  const decoded = jwt.decode(token);
  const alg = config.get('jwt').ALGORITHM;
  t.deepEqual(decoded.header, { alg, typ: 'JWT' });
});

test('it should verify a token given a valid token', t => {
  const token = jwt.sign(t.context.payload);
  const decoded = jwt.verify(token);
  t.is(decoded.id, t.context.payload.id);
});

test('it should decode a token given a valid token', t => {
  const token = jwt.sign(t.context.payload);
  const decoded = jwt.decode(token);
  t.is(decoded.payload.id, t.context.payload.id);
});
