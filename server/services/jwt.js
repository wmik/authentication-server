const path = require('path');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { shallowMerge } = require('../util');
let { privateKey, publicKey } = require('../util/keygen');

if (process.env.NODE_ENV === 'production') {
  privateKey = fs.readFileSync(path.resolve(process.env.PRIVATE_KEY_FILEPATH));
  publicKey = fs.readFileSync(path.resolve(process.env.PUBLIC_KEY_FILEPATH));
}
const { ISSUER, SUBJECT, AUDIENCE, EXPIRY, ALGORITHM } = config.get('jwt');

const defaultOptions = {
  issuer: ISSUER,
  audience: AUDIENCE,
  subject: SUBJECT
};

function sign(payload, options) {
  const defaultSignOptions = shallowMerge(defaultOptions, {
    expiresIn: EXPIRY,
    algorithm: ALGORITHM
  });
  return jwt.sign(
    payload,
    privateKey,
    shallowMerge(defaultSignOptions, options)
  );
}

function verify(token, options) {
  const defaultVerifyOpts = shallowMerge(defaultOptions, {
    algorithms: [ALGORITHM]
  });
  try {
    return jwt.verify(
      token,
      publicKey,
      shallowMerge(defaultVerifyOpts, options)
    );
  } catch (error) {
    return false;
  }
}

function decode(token) {
  return jwt.decode(token, { complete: true });
}

module.exports = { sign, verify, decode };
