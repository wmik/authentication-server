> # authentication-server

[RFC6749](https://tools.ietf.org/html/rfc6749) compliant authentication server

## Folder structure

|Module|Description|
|-|-|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [.bin](.bin)|Network entry point.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [app](app)|Application entry point.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [config](config)|Application configuration.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/folder.svg" width="15" height="15" /> [database](database)|Application data related objects.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [.gitignore](.gitignore)|Git file exclusion.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [.gitlab-ci.yml](.gitlab-ci.yml)|Gitlab CI pipeline configuration.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [ecosystem.config.js](ecosystem.config.js)|PM2 configuration file.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [license](license)|License file.|
|<img src="https://cdn.rawgit.com/danklammer/bytesize-icons/master/dist/icons/file.svg" width="15" height="15" /> [Procfile](Procfile)|Heroku startup script.|

## Table of contents
- [Installation and setup](docs/installation.md)

## License
MIT &copy; 2019
