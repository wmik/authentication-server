import React from 'react';
import { Divider, Button } from 'semantic-ui-react';
import { AppDetailsForm } from './OAuthAppForms';
import { HeaderContainer } from './header-provider';

export default function OAuthAppDetails({ location }) {
  const { setHeader } = HeaderContainer.useContainer();
  const { app } = location.state;
  const { name } = app;
  React.useEffect(() => {
    setHeader(name);
  }, [setHeader, name]);
  return (
    <React.Fragment>
      <p>{app.description}</p>
      <Divider />
      <AppDetailsForm callToAction="Update application" data={app}>
        <Button color="red">Delete application</Button>
      </AppDetailsForm>
    </React.Fragment>
  );
}
