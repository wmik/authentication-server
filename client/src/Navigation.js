import React from 'react';
import { Link } from '@reach/router';
import { AuthContainer } from './auth-provider';
import { Dropdown, Grid, Menu, Icon, Button } from 'semantic-ui-react';

export function TopNavigation() {
  const auth = AuthContainer.useContainer();
  const handleLogout = React.useCallback(() => {
    auth.setLogin(false);
    auth.setAccount({});
  }, [auth]);
  return (
    <Grid.Row>
      <Grid.Column>
        <Menu borderless>
          <Menu.Item header>OAuth2 Admin</Menu.Item>
          <Menu.Item position="right">
            <Button type="button" basic icon>
              <Icon name="user circle" size="large" />
              <Dropdown simple>
                <Dropdown.Menu
                  direction="left"
                  style={{ width: '12rem', padding: '0.32rem' }}
                >
                  <Dropdown.Item text="Account" />
                  <Dropdown.Divider />
                  <Dropdown.Item text="logout" onClick={handleLogout} />
                </Dropdown.Menu>
              </Dropdown>
            </Button>
          </Menu.Item>
        </Menu>
      </Grid.Column>
    </Grid.Row>
  );
}

export function SideNavigation() {
  const [activeItem, setActiveItem] = React.useState('');
  const handleItemClick = React.useCallback(
    e => setActiveItem(e.currentTarget.innerText.toLowerCase()),
    []
  );
  return (
    <Grid.Column width={4}>
      <Menu vertical>
        <Menu.Item>
          <Menu.Header>Applications</Menu.Header>
          <Menu.Menu>
            <Link to="/apps/installed">
              <Menu.Item
                as="span"
                active={activeItem === 'installed'}
                onClick={handleItemClick}
              >
                Installed
              </Menu.Item>
            </Link>
            <Link to="/apps/register">
              <Menu.Item
                as="span"
                active={activeItem === 'register'}
                onClick={handleItemClick}
              >
                Register
              </Menu.Item>
            </Link>
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Settings</Menu.Header>
          <Menu.Menu>
            <Link to="/settings/account">
              <Menu.Item
                as="span"
                active={activeItem === 'account'}
                onClick={handleItemClick}
              >
                Account
              </Menu.Item>
            </Link>
          </Menu.Menu>
        </Menu.Item>
      </Menu>
    </Grid.Column>
  );
}
