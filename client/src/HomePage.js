import React from 'react';
import { Router } from '@reach/router';
import { Grid, Header, Divider } from 'semantic-ui-react';
import { TopNavigation, SideNavigation } from './Navigation';
import { AppRegistrationForm } from './OAuthAppForms';
import InstalledOAuthAppList from './InstalledOAuthAppList';
import OAuthAppDetails from './OAuthAppDetails';
import { HeaderContainer } from './header-provider';
import AccountDetails from './AccountDetails';

export function MainContent({ children }) {
  const { header } = HeaderContainer.useContainer();
  return (
    <Grid.Column width={10}>
      <Header as="h2">{header}</Header>
      <Divider />
      {children}
    </Grid.Column>
  );
}

export default function HomePage() {
  return (
    <Grid padded>
      <TopNavigation />
      <Grid.Row>
        <SideNavigation />
        <HeaderContainer.Provider>
          <MainContent>
            <Router>
              <AppRegistrationForm path="apps/register" />
              <InstalledOAuthAppList path="apps/installed" />
              <OAuthAppDetails path="apps/installed/:id" />
              <AccountDetails path="settings/account" />
            </Router>
          </MainContent>
        </HeaderContainer.Provider>
      </Grid.Row>
    </Grid>
  );
}
