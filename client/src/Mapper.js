import React from 'react';

export default function Mapper({ children, from }) {
  const child = React.Children.only(children);
  const mapped = from.map((props, idx) =>
    React.cloneElement(
      child,
      Object.assign(
        {
          key: props.id || idx
        },
        props
      )
    )
  );
  return React.createElement(React.Fragment, null, mapped);
}
