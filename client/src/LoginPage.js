import React from 'react';
import { AuthContainer } from './auth-provider';
import { Form, Grid, Header, Button, Message } from 'semantic-ui-react';
import { useField, useForm } from 'react-jeff';

export default function LoginPage({ navigate }) {
  const auth = AuthContainer.useContainer();
  const [error, setError] = React.useState(false);
  React.useEffect(() => {
    if (auth.isLoggedIn) {
      navigate('/');
    }
  }, [auth.isLoggedIn, navigate]);
  function _handleSubmit(username, password) {
    return async function onSubmit() {
      try {
        const response = await window.fetch('', {
          method: 'POST',
          headers: {
            Authorization: `Basic ${btoa(`${username}:${password}`)}`
          }
        });
        if (response.ok) {
          const { data } = await response.json();
          auth.setAccount(data.account);
          auth.setLogin(response.ok);
          return data;
        }
        setError(true);
      } catch (err) {
        setError(true);
      }
    };
  }
  const handleSubmit = React.useCallback(_handleSubmit, []);
  return (
    <Grid padded stackable>
      <Grid.Row centered>
        <LoginForm handleSubmit={handleSubmit} error={error} />
      </Grid.Row>
    </Grid>
  );
}

export function LoginForm({ error, handleSubmit }) {
  const username = useField({
    defaultValue: '',
    required: true
  });
  const password = useField({
    defaultValue: '',
    required: true
  });
  const form = useForm({
    fields: [username, password],
    onSubmit: handleSubmit(username.value, password.value)
  });
  const onChange = React.useCallback(
    handler => e => handler(e.currentTarget.value),
    []
  );
  return (
    <Grid.Column width={4}>
      <Form
        error={error}
        loading={form.submitting}
        onSubmit={e => {
          e.preventDefault();
          form.props.onSubmit();
        }}
      >
        <Header as="h2" dividing>
          Login
        </Header>
        <Message
          error
          header="Invalid credentials"
          content="Please confirm your username/email and password"
        />
        <Form.Input
          label="Username"
          icon="user"
          iconPosition="left"
          value={username.props.value}
          onChange={onChange(username.props.onChange)}
        />
        <Form.Input
          label="Password"
          type="password"
          icon="lock"
          iconPosition="left"
          value={password.props.value}
          onChange={onChange(password.props.onChange)}
        />
        <Form.Checkbox label="Remember me" />
        <Button type="submit" fluid>
          Login
        </Button>
      </Form>
    </Grid.Column>
  );
}
