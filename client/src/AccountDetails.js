import React from 'react';
import { Form } from 'semantic-ui-react';
import { AuthContainer } from './auth-provider';
import { HeaderContainer } from './header-provider';

export default function AccountDetails() {
  const auth = AuthContainer.useContainer();
  const header = HeaderContainer.useContainer();
  React.useEffect(() => {
    header.setHeader('Account settings');
  }, [header]);
  const { email } = auth.account;
  return (
    <Form>
      <Form.Input label="Email address" value={email} />
    </Form>
  );
}
