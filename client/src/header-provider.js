import React from 'react';
import { createContainer } from 'unstated-next';

export function useHeaderProvider() {
  const [header, setHeader] = React.useState('');
  return { header, setHeader };
}

export const HeaderContainer = createContainer(useHeaderProvider);
