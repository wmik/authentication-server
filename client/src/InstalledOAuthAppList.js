import React from 'react';
import { Divider, List, Button } from 'semantic-ui-react';
import Mapper from './Mapper';
import { Link } from '@reach/router';
import { HeaderContainer } from './header-provider';

const apps = {
  1: {
    id: 1,
    name: 'App #1',
    description: 'Application one description',
    homepage_url: 'http://app-1.com',
    redirect_url: 'http://app-1.com/callback'
  },
  2: {
    id: 2,
    name: 'App #2',
    description: 'Application two description',
    homepage_url: 'http://app-2.com',
    redirect_url: 'http://app-2.com/callback'
  }
};

export default function InstalledOAuthAppList() {
  const { setHeader } = HeaderContainer.useContainer();
  React.useEffect(() => {
    setHeader('Installed OAuth apps');
  }, [setHeader]);
  const appList = React.useMemo(() => Object.values(apps), []);
  return (
    <List verticalAlign="middle" selection>
      <Mapper from={appList}>
        <ApplicationTile />
      </Mapper>
    </List>
  );
}

export function ApplicationTile({ id, name, description }) {
  return (
    <React.Fragment>
      <List.Item style={{ padding: '0.64rem' }}>
        <Link to={`/apps/installed/${id}`} state={{ app: apps[id] }}>
          <List.Content floated="right">
            <Button>Configure</Button>
          </List.Content>
          <List.Content header={name} description={description} />
        </Link>
      </List.Item>
      <Divider />
    </React.Fragment>
  );
}
