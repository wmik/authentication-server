import React from 'react';
import { Form, Button, TextArea } from 'semantic-ui-react';
import { HeaderContainer } from './header-provider';

export function AppRegistrationForm() {
  const { setHeader } = HeaderContainer.useContainer();
  React.useEffect(() => {
    setHeader('Register a new OAuth application');
  }, [setHeader]);
  return <AppDetailsForm callToAction="Register application" />;
}

export function AppDetailsForm({ callToAction, children, data = {} }) {
  return (
    <Form>
      <Form.Input
        label="Application name"
        width={8}
        required
        value={data.name}
      />
      <Form.Input
        label="Homepage URL"
        width={8}
        placeholder="http://www.yourapp.com/"
        required
        value={data.homepage_url}
      />
      <Form.Field
        control={TextArea}
        label="Application description"
        value={data.description}
      />
      <Form.Input
        label="Authorization callback/redirect URL"
        width={8}
        placeholder="http://www.yourapp.com/redirect_uri"
        required
        value={data.redirect_url}
      />
      <Button>{callToAction}</Button>
      {children}
    </Form>
  );
}
