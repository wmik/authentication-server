import { createContainer } from 'unstated-next';
import useLocalStorage from 'react-use/lib/useLocalStorage';

export function useAuthProvider() {
  const [isLoggedIn, setLogin] = useLocalStorage('logged-in', false);
  const [account, setAccount] = useLocalStorage('account', {});
  return { isLoggedIn, setLogin, account, setAccount };
}

export const AuthContainer = createContainer(useAuthProvider);
