import 'semantic-ui-css/semantic.min.css';
import React from 'react';
import { Router, Redirect } from '@reach/router';
import { AuthContainer } from './auth-provider';
import LoginPage from './LoginPage';
import HomePage from './HomePage';

function Protected({ children, path, redirectUri }) {
  const auth = AuthContainer.useContainer();
  if (auth.isLoggedIn) {
    return <React.Fragment>{children}</React.Fragment>;
  }
  return <Redirect from={path} to={redirectUri} noThrow />;
}

function App() {
  return (
    <AuthContainer.Provider>
      <Router>
        <Protected path="/" redirectUri="/login">
          <HomePage path="/*" />
        </Protected>
        <LoginPage path="/login" redirectUri="/" />
      </Router>
    </AuthContainer.Provider>
  );
}

export default App;
